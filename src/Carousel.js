import React from "react";
import img from "./gambar1.png";
import { Carousel } from "react-responsive-carousel";

export default () => (
  <Carousel autoPlay>
    <div>
      <img src={img} className="img1" alt="img1" />;
      <p className="legend">img 1</p>
    </div>
    <div>
      <img src={img} className="img2" alt="img2" />;
      <p className="legend">img 2</p>
    </div>
    <div>
      <img src={img} className="img3" alt="img3" />;
      <p className="legend">img 3</p>
    </div>
    <div>
      <img src={img} className="img4" alt="img4" />;
      <p className="legend">img 4</p>
    </div>
    <div>
      <img src={img} className="img5" alt="img5" />;
      <p className="legend">img 5</p>
    </div>
    <div>
      <img src={img} className="img6" alt="img6" />;
      <p className="legend">img 6</p>
    </div>
  </Carousel>
);
